<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')/sVF Fha)oZP31(hEBCdUv#YxzV09b|Q?4(a8E[`XQ#eN,rHO!OF+3wYh5MhWf+');
define('SECURE_AUTH_KEY',  'iNBDo@.&bnvqJF80hE(wm]UCMXIBbsvLRmbLE[.GVJ@:|}5oltgRc<z+~_AN*|iV');
define('LOGGED_IN_KEY',    '3uBj8PkCKve!/|od|zQ`,g/_W9lQ|0{O`+P.5Vu`5p%+Nt)JE$OY$YP,dPBj2UO_');
define('NONCE_KEY',        '-!M<1H(&i(^voO<62^ii;mW{x|/{H6GtSi]pP;IJId8kGn#-|Um|UWrk<Y?(m%`-');
define('AUTH_SALT',        'ZUc-rc3kGz|$x;SuYP3zF<&( 2<a[3S]d%#MLOLUO4CC7riH>I;@)$uh^JMQt*M2');
define('SECURE_AUTH_SALT', '/|YM9UaspZ|D.*Rh0O&bLu--`nt8`)$w.#rGaP@G/D7Qg-l.2-+@4^4n5`El=`~n');
define('LOGGED_IN_SALT',   '5H2&R)T+_3m 0wV*/A]uKsf_u*NY6zo[Qmkd${R;O]h/;CIoOKL%a2F|]<%L~_v^');
define('NONCE_SALT',       'T;o@5[$oe8oPSjD^,KcMZhfH|*-Em- }z/3|G?~gB]Ros,S|6ky}lBHiwGU7a|P?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
