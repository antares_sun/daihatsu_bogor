    <footer>
      <p>&copy; 2017 Daihatsu Bogor</p>
      <?php //wp_nav_menu(array('theme_location' => 'secondary')); ?>
    </footer>
    <?php wp_footer(); ?>

    <script type="text/javascript">  
      var mySwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        
        // Navigation arrows
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
      });
    </script>
  </body>
</html>