<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daihatsu Bogor</title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" />
    
    <?php wp_head(); ?>
  </head>
  <?php 
    if(is_front_page()):
      $ridestyle_classes = array('my-class');
    else:
      $ridestyle_classes = array('no-my-class');
    endif;
  ?>
  <body <?php body_class($ridestyle_classes); ?>>
    <div class="navWrapper">
      <div class="logo">
        <a href="<?php echo get_home_url(); ?>"><img src="<?php header_image(); ?>" alt="Logo Daihatsu"></a>
      </div>
      <div class="main-navigation">
        <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
        <div class="searching">
          <!-- <i class="fa fa-search main-search"></i> -->
          <i id="main-search" class="fa fa-search inactive-main-search"></i>
          <div class="hide-opacity">
            <form id="main-searchForm" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
              <input id="main-searchNav" type="search" class="form-control" placeholder="Search ... " value="<?php echo get_search_query() ?>" name="s" title="Search" autocomplete="off"/>
            </form>
            <i id="fa-close" class="fa fa-close inactive-main-search"></i>
            <!-- <input type="submit" style="opacity: 0;"/>           -->
          </div>
        </div>
      </div>      
    </div>
    
