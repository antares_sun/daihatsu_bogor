<?php
/*
  Template Name: Home Page
*/
get_header(); ?>
<?php
  // PRINT OTHER 2 POST EXECPT THE FIRST ONE
  $args_slider = array(
    'type' => 'post',
    'posts_per_page' => 1
  );
  $lastBlog_slider = new WP_Query($args_slider);
  
  if( $lastBlog_slider->have_posts() ):
    while( $lastBlog_slider->have_posts() ): $lastBlog_slider->the_post(); ?>
      <?php get_template_part('content', 'slider'); ?>
    <?php endwhile;
  endif;

  wp_reset_postdata(); 
?>
<!-- <div class="diagonal"></div> -->
<div class="container">
  <div id="first-main" class="col-12 col-m-12 main-featured">
    <div class="row home-grid">
      <div class="category-title">
        <?php
          $category_id = get_cat_ID( 'Promo' );
          $category_link = get_category_link( $category_id );
        ?>
        <a href="<?php echo esc_url( $category_link ); ?>" title="Promo Mobil Daihatsu">
          <div id="promo" class="circle-icon"></div>  
        </a>      
        <p>Promo</p>
      </div>
      <?php
        $args = array(
          'type' => 'post',
          'posts_per_page' => 6,
          'offset' => 1
        );
        $lastBlog = new WP_Query($args);
        
        if( $lastBlog->have_posts() ):
          while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
            <?php get_template_part('content', 'promo'); ?>
           <?php endwhile;
        endif;

        wp_reset_postdata();
      ?>
    </div> 
    <div class="row">
      <a href="<?php echo esc_url( $category_link ); ?>" class="more" title="Promo Mobil Daihatsu">
        LIHAT SEMUA PROMO
      </a>
    </div>
  </div>  
</div>
<div class="divider">
  <div id="credit-form" class="container">
    <h1 class="title">SIMULASI KREDIT</h1>
    <div class="main-caption"></div>
    <?php wd_form_maker(5); ?>
  </div>
</div>
<div class="divider">
  <div id="quotes" class="swiper-container">
    <h1 class="title">TESTIMONI</h1>
    <div class="main-caption"></div>
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <blockquote>
          <p>Cars and cameras are the two things I let myself be materialistic about. I don't care about other stuff.</p>
          <p>Louis C. K.</p>
        </blockquote>
      </div>
      <div class="swiper-slide">
        <blockquote class="quotes">
          <p>The cars we drive say a lot about us.</p>
          <p>Alexandra Paul</p>
        </blockquote>
      </div>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
  </div>
</div>
<div class="container sales-contact">
  <div class="col-12 col-m-12 main-featured">
    <div class="row home-grid">
      <div class="category-title">
        <a href="javascript:void(0);" title="Info">
          <div id="info" class="circle-icon"></div>  
        </a>      
        <p>SALES EXECUTIVE</p>
      </div>
    </div> 
    <div class="row">
      <div class="col-6">
        <figure>
          <img src="./wp-content/uploads/2017/01/sales.jpg">
        </figure>
      </div>
      <div class="col-6">
        <ul>
          <li>Nama:</li>
          <li class="contact-list">Alfin</li>
          <li>KARTU HALO:</li>
          <li class="contact-list">081319961186</li>
          <li>BBM:</li>
          <li class="contact-list">2ADFE9E9</li>
          <li>ALAMAT:</li>
          <li class="contact-list">.....</li>
        </ul>
      </div>
    </div>
  </div>  
</div>
<!-- <div class="diagonal reverse"></div> -->
<!-- <div class="divider"></div> -->
<!-- <div class="diagonal last"></div> -->

<div id="back-to-top"></div>
<?php get_footer(); ?>