<div class="col-5 no-padding">
<?php
  $args = array(
    'type' => 'post',
    'posts_per_page' => 2,
    'offset' => 1
  );
  $lastBlog = new WP_Query($args);
  
  if( $lastBlog->have_posts() ):
    while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
      <article id="post-<?php the_ID(); ?>" class="col-12 col-m-6 article-grid no-padding bike">
        <figure class="img-grid">    
          <div class="cover-caption">
            <?php
              $current_cat = "";
              $categories = get_the_category();

              foreach($categories as $category) { 
                if($category->cat_name == 'News') {
                  $current_cat = 'car-white';
                } else if ($category->cat_name == 'Tutorial') {
                  $current_cat = 'bike-white';
                }
              }
            ?>
            <div class="popular-header">
              <div id="<?php echo $current_cat; ?>" class="main-caption"></div>
              <i class="date-post" style="color: #fff; display: block;margin-top: 5px;"><?php the_time('F j, Y'); ?></i>
              <?php the_title( sprintf('<span><a href="%s">', esc_url( get_permalink() ) ),'</a></span>' ); ?><br/>
            </div>
          </div> 
          <?php if(has_post_thumbnail()) :
            $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 400, 300 ), false, '' ); ?>
            <a href="<?php echo esc_url(get_permalink()); ?>">
              <img src="<?php echo $src[0]; ?>" alt="Featured Posts" />
            </a>
          <?php endif; ?>
        </figure>
      </article>
    <?php endwhile;
  endif;

  wp_reset_postdata();
?>
</div>
<div class="col-2 no-padding">
<?php
  $args = array(
    'type' => 'post',
    'posts_per_page' => 2,
    'offset' => 3
  );
  $lastBlog = new WP_Query($args);
  
  if( $lastBlog->have_posts() ):
    while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
      <article id="post-<?php the_ID(); ?>" class="col-12 col-m-6 article-grid no-padding">
        <figure class="img-grid">   
          <?php if(has_post_thumbnail()) :
            $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 400, 300 ), false, '' ); ?>
            <a href="<?php echo esc_url(get_permalink()); ?>">
              <img src="<?php echo $src[0]; ?>" alt="Featured Posts" />
            </a>
          <?php endif; ?>
        </figure>
        <div class="grid-padding">
          <i class="date-post"><?php the_time('F j, Y'); ?></i>
          <?php the_title( sprintf('<p class="featured-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></p>' ); ?>
        </div> 
      </article>
    <?php endwhile;
  endif;

  wp_reset_postdata();
?>
</div>
<div class="col-5 no-padding">
<?php
  $args = array(
    'type' => 'post',
    'posts_per_page' => 2,
    'offset' => 5
  );
  $lastBlog = new WP_Query($args);
  
  if( $lastBlog->have_posts() ):
    while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
      <article id="post-<?php the_ID(); ?>" class="col-12 col-m-6 article-grid no-padding bike">
        <figure class="img-grid">
          <div class="cover-caption">
            <?php
              $current_cat = "";
              $categories = get_the_category();

              foreach($categories as $category) { 
                if($category->cat_name == 'News') {
                  $current_cat = 'car-white';
                } else if ($category->cat_name == 'Tutorial') {
                  $current_cat = 'bike-white';
                }
              }
            ?>
            <div class="popular-header">
              <div id="<?php echo $current_cat; ?>" class="main-caption"></div>
              <i class="date-post" style="color: #fff; display: block;margin-top: 5px;"><?php the_time('F j, Y'); ?></i>
              <?php the_title( sprintf('<span><a href="%s">', esc_url( get_permalink() ) ),'</a></span>' ); ?><br/>
            </div>
          </div>  
          <?php if(has_post_thumbnail()) :
            $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 400, 300 ), false, '' ); ?>
            <a href="<?php echo esc_url(get_permalink()); ?>">
              <img src="<?php echo $src[0]; ?>" alt="Featured Posts" />
            </a>
          <?php endif; ?>
        </figure>
      </article>
    <?php endwhile;
  endif;

  wp_reset_postdata();
?>
</div>