<?php 
  $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 1024, 409 ), false, '' );
?>
<section class="home-slider">
  <img src="<?php echo $src[0]; ?>" alt="<?php echo get_permalink(); ?>" />
  <div class="wrapper-container">
    <div class="header-slider">
      <?php
        $current_cat = "";
        $categories = get_the_category();

        foreach($categories as $category) { 
          if($category->cat_name == 'News') {
            $current_cat = 'car-white';
          }
        }
      ?>
      
      <?php the_title( sprintf('<h1 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
      <div id="<?php echo $current_cat; ?>" class="main-caption"></div>
      <p><?php echo wp_trim_words( get_the_content(), 10, '...' );; ?></p>
    </div>
  </div>
</section>

<!-- <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /> -->