<?php 
  // Insert CSS & JS
  function ridestyle_script_enqueue() {
    // CSS
    wp_enqueue_style('customstyle', get_template_directory_uri().'/css/main.css', array(), '1.0.0', 'all');
    wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
    wp_enqueue_style('swipercss', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css');
    
    // JS
    wp_enqueue_script('jquery', get_template_directory_uri().'/js/jquery.min.js');
    wp_enqueue_script('customjs', get_template_directory_uri().'/js/main.js', array(), '1.0.0', true);
    wp_enqueue_script('swiperjs', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js');
    // wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', array(), '1.12.0', true);
  }
  add_action('wp_enqueue_scripts', 'ridestyle_script_enqueue');

  //  Menu setup
  function ridestyle_theme_setup() {
    add_theme_support('menus');

    register_nav_menu('primary', 'Primary Header Navigation');
    register_nav_menu('secondary', 'Footer Navigation');
  }
  add_action('init', 'ridestyle_theme_setup');

  // Theme support function
  add_theme_support('custom-background');
  add_theme_support('custom-header');
  add_theme_support('post-thumbnails');
  add_theme_support('post-formats', array('aside', 'image', 'video'));

  // Sidebar function
  function ridestyle_widget_setup() {
    register_sidebar(
      array(
        'name' => 'Sidebar 1',
        'id' => 'sidebar-1',
        'class' => 'custom',
        'description' => 'Standard Sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>'
      )
    );
    register_sidebar(
      array(
        'name' => 'Sidebar Home',
        'id' => 'sidebar-home',
        'class' => 'custom',
        'description' => 'Home Sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>'
      )
    );
  }
  add_action('widgets_init', 'ridestyle_widget_setup');
?>