<?php
  $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 400, 300 ), false, '' ); 
?>

<div class="swiper-slide">
  <div class="popular-header">
    <?php
      $current_cat = "";
      $categories = get_the_category();

      foreach($categories as $category) { 
        if($category->cat_name == 'News') {
          $current_cat = 'car-white';
        }
      }
    ?>
    <div id="<?php echo $current_cat; ?>" class="main-caption"></div>
    <?php the_title( sprintf('<span><a href="%s">', esc_url( get_permalink() ) ),'</a></span>' ); ?><br/>
    <i class="fa fa-eye capt-icon"></i><span><?php echo get_post_views(get_the_ID()); ?></span>
  </div>
  <img src="<?php echo $src[0]; ?>" alt="Popular Posts" class="swiper-image"/>
</div>
    