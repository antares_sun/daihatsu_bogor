<?php get_header(); ?>
<div class="container">
  <div class="col-12 col-m-12">
    <div class="row">
      <div class="col-8 col-m-8">  
        <?php 
          if( have_posts() ):
            while( have_posts() ): the_post(); ?>
              <h2> <?php the_title(); ?> </h2>
              <div class="thumbnail-img"><?php the_post_thumbnail('medium'); ?></div>
              <small>Posted on: <?php the_time('F j, Y'); ?>, at <?php the_time('g:i a'); ?>, in <?php the_category(); ?></small>
              <?php the_content(); ?>
              <?php echo get_post_views(get_the_ID());?>
              <hr>
            <?php endwhile;
          endif;
        ?>
      </div>
      <div class="col-4 col-m-4">
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>