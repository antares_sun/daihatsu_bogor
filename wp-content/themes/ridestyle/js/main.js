;(function($){
  var init = (function(){
    console.log($('.grid-padding').height());
    var main = function(){
      $('#fa-close').click(function(){
        $('.main-search').show();
        $('#formSearh').hide();
        $('.hide-opacity').stop().css('opacity', '0');     
        $('.searching').css({
          'left': '100%',
          'margin-left': '-70px'
        });
        $('#main-searchNav').val(""); 
      });

      $('#main-search').click(function(){
        $('.main-search').hide();
        $('#formSearh').show();
        $('.hide-opacity').delay(100).animate({'opacity': '1'}, 100);
        $('.searching').css({
          'left': '0%',
          'margin-left': '0px'
        });       
        $('#main-searchNav').focus();
      });

      // $('.home-grid').find('article').first().removeClass('col-6 col-m-6').css({
      //   padding: 5,
      //   width: '100%',
      //   float: 'left'
      // });      
  
      // $('#main-submit').click(function(){        
      //   $('input[type=submit]').trigger('click');
      // });
      $('article.bike').hover(
        function(){
          $(this).find('.cover-caption').css({
            opacity: 1,
            top: '0%',
          });
          $(this).find('img').css({
            transform: 'scale(1)',
            opacity: 1
          });
        },
        function(){
          $(this).find('.cover-caption').css({
            opacity: 0,
            top: '-100%'
          });
          $(this).find('img').css({
            transform: 'scale(1)',
            opacity: 1
          });
        }
      );

      resize('.diagonal');
      $(window).on('resize', function(){
        resize('.diagonal');
      });

      scroll();
    },
    scroll = function(){
      $(window).scroll(function() {
        if($('.navWrapper').offset().top > 300) {
          $('.navWrapper').css('height', 50);
          // $('.searching').css('height', 50);
          $('.logo img').css('width', '50%');
          $('#main-search, #fa-close').css({
            'height': 50,
            'font-size': 20,
            'line-height': '50px'
          });
          $('#main-searchNav').css('height', 50);

          $('#back-to-top').css({
            opacity: 1,
            bottom: 20
          });
        } else {
          $('.navWrapper').css('height', 70);
          // $('.searching').css('height', 70);
          $('.logo img').css('width', '65%');
          $('#main-search, #fa-close').css({
            'height': 70,
            'font-size': 25,
            'line-height': '66px'
          });
          $('#main-searchNav').css('height', 70);

          $('#back-to-top').css({
            opacity: 0,
            bottom: -20
          });
        }
      });

      $('#back-to-top').click(function(){
        $('html, body').animate({
          scrollTop: 0
        }, 1500);
      });
    },
    resize = function(diagonal){
      var widthWindow = $(window).width();
      var heightWindow = $(window).height();

      $(diagonal).css({
        borderLeftWidth: widthWindow
      });     
    }

    return {
      main : main
    }
  })();

  init.main();
})(jQuery);