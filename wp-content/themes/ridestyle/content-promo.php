<article id="post-<?php the_ID(); ?>" class="col-4 col-m-6 article-grid">
  <figure class="img-grid">    
    <?php if(has_post_thumbnail()) :
      $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 400, 300 ), false, '' ); ?>
      <a href="<?php echo esc_url(get_permalink()); ?>">
        <!-- <div class="cover-caption"></div> -->
        <img src="<?php echo $src[0]; ?>" alt="Featured Posts" />
        <div class="image_car" data-image="<?php echo $src[0]; ?>"></div>
      </a>
    <?php endif; ?>
  </figure>
  <div class="grid-padding">
    <i class="date-post"><?php the_time('F j, Y'); ?></i>
    <?php the_title( sprintf('<p class="featured-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></p>' ); ?>
  </div>  
</article>

<script type="text/javascript">
  var image_car = document.querySelectorAll('.image_car')[0];    
  var val = image_car.getAttribute('data-image');
  image_car.style.background = "url(" + val + ")";
</script>